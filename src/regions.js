import * as React from "react";
import { List, Datagrid, TextField } from 'react-admin';

export const RegionList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="libelle" />
        </Datagrid>
    </List>
)