import * as React from "react";
import { Admin, Resource } from 'react-admin';

import jsonServerProvider from 'ra-data-json-server';
//import simpleRestProvider from 'ra-data-simple-rest';

import { RegionList } from './regions';
import { CentreList, CentreEdit, CentreCreate, CentreIcon } from './centres';
import { BanqueList, BanqueEdit, BanqueCreate, BanqueIcon } from './banques';


const dataProvider = jsonServerProvider('http://localhost:8081/api');

const App = () => (
<Admin dataProvider={dataProvider} title="Example Admin">
  <Resource name="regions" list={RegionList}/>
  <Resource name="centres" list={CentreList} edit={CentreEdit} create={CentreCreate} icon={CentreIcon}/>
  <Resource name="banques" list={BanqueList} edit={BanqueEdit} create={BanqueCreate} icon={BanqueIcon}/>
</Admin>


);

export default App;