import * as React from "react";
import { Create, Edit, SimpleForm, TextInput, List, Datagrid, TextField, ReferenceInput, SelectInput, required } from 'react-admin';
import HomeIcon from '@material-ui/icons/Home';
export const CentreIcon = HomeIcon;

export const CentreList = (props) => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField label="ID" source="id" />
            <TextField label="LIBELLE" source="libelle" />
            <TextField label="REGION" source="idregion" />
        </Datagrid>
    </List>
);

export const CentreCreate = (props) => (
    <Create {...props} title="Ajout Centre">
        <SimpleForm>
            <TextInput label="LIBELLE" source="libelle" validate={[required()]}/>
            <ReferenceInput label="REGION" source="idregion" reference="regions" validate={[required()]}>
                <SelectInput optionText="libelle" />
            </ReferenceInput>
        </SimpleForm>
    </Create>
);

export const CentreEdit = (props) => (
    <Edit {...props} title="Modification Centre">
        <SimpleForm>
            <TextInput disabled label="ID" source="id" />
            <TextInput label="LIBELLE" source="libelle" validate={[required()]}/>
            <ReferenceInput label="REGION" source="idregion" reference="regions" validate={[required()]}>
                <SelectInput optionText="libelle" />
            </ReferenceInput>
        </SimpleForm>
    </Edit>
);
