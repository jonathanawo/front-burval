import * as React from "react";
import { Create, Edit, List, SimpleForm, Datagrid, TextField, TextInput, required} from 'react-admin';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
export const BanqueIcon = AccountBalanceIcon;

export const BanqueList = (props) => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="id" />
            <TextField source="libelle" />
        </Datagrid>
    </List>
)

export const BanqueCreate = (props) => (
    <Create {...props} title="Ajout Banque">
        <SimpleForm>
            <TextInput source="libelle" validate={[required()]}/>
        </SimpleForm>
    </Create>
);

export const BanqueEdit = (props) => (
    <Edit {...props} title="Modification Banque">
        <SimpleForm>
            <TextInput disabled label="Id" source="id" />
            <TextInput source="libelle" validate={[required()]}/>
        </SimpleForm>
    </Edit>
);