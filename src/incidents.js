import * as React from "react";
import { List, Datagrid, TextField } from 'react-admin';

export const IncidentList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="libelle" />
        </Datagrid>
    </List>
)